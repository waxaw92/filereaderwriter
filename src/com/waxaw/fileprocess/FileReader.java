package com.waxaw.fileprocess;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.waxaw.fileprocess.process.FileProcessorBuilder;
import com.waxaw.fileprocess.process.FileProcessor;

/**
 * <b>Clase principal.</b><br /><br />
 * Emplea la interfaz {@link FileProcessor} y todas sus implementaciones para
 * procesamiento de archivos.
 * 
 * @author wgavidia
 * @version 1.0 {@code (2018-12-26)}
 */
public class FileReader {

	/**
	 * M&eacute;todo main para ejecuci&oacute;n del <i><b>jar</b></i>
	 * 
	 * @param args Argumentos para ejecuci&oacute;n. <i>No requeridos</i>.
	 */
	public static void main(String... args) {
		Path file = Paths.get("C:\\Users\\wgavidic\\Documents\\test.txt");
		FileProcessor processFile = FileProcessorBuilder.build(FileProcessor.FILE_VALIDATOR);
		processFile.processFile(file, ",", 3);
	}

}
