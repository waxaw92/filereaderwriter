package com.waxaw.fileprocess.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * Clase que permite leer un archivo y obtener las diferentes apariciones de un
 * fragmento de texto.
 * 
 * @author wgavidia
 * @version 2.0 {@code (2019-01-02)}
 */
public class FileReaderDistinct implements FileProcessor {

	private static final long serialVersionUID = 2L;

	/**
	 * Constructor de la clase
	 */
	protected FileReaderDistinct() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processFile(Path file, Object... args) {
		try (BufferedReader buffer = Files.newBufferedReader(file)) {
			if (args == null || args.length < 2 || !(args[0] instanceof Integer))
				throw new RuntimeException("args list required Integer positions (start, end) to count.");

			final Integer start = Integer.valueOf(args[0].toString());
			final Integer end = Integer.valueOf(args[1].toString());
			Map<String, Integer> data = new HashMap<>();

			buffer.lines().filter(l -> l != null && !l.isEmpty() && l.length() > end).forEach(l -> {
				String line = l.substring(start, end);
				if (data.containsKey(line))
					data.put(line, data.get(line) + 1);
				else
					data.put(line, 1);
			});

			data.forEach((k, v) -> System.out.println(k + ": " + v));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
