package com.waxaw.fileprocess.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Clase para validar los archivos en un directorio y comprobar si contienen un
 * caracter o texto espec&iacute;fico.
 * 
 * @author wgavidia
 * @version 2.0 {@code (2019-01-02)}
 */
public class FilesValidator implements FileProcessor {

	private static final long serialVersionUID = 3L;

	/**
	 * Constructor por defecto
	 */
	protected FilesValidator() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processFile(Path file, Object... args) {
		if (args == null || args.length < 1 || !(args[0] instanceof String))
			throw new RuntimeException("args params list incomplete. Required String value.");

		String strToFind = String.valueOf(args[0]);

		File dir = file.toFile();
		if (!dir.exists() || !dir.isDirectory()) {
			System.out.println("El path no corresponde a un directorio del sistema");
		}

		File[] files = dir.listFiles();
		for (File f : files) {
			validateFile(strToFind, f.toPath());
		}
		System.out.println("Archivos procesados satisfactoriamente.");
	}

	/**
	 * M&eacute;todo para verificar las coincidencias de un caracter o texto en un
	 * archivo.
	 * 
	 * @param strToFind {@link String} caracter o texto a buscar.
	 * @param file      {@link Path} archivo a validar.
	 */
	public void validateFile(String strToFind, Path file) {
		try (BufferedReader buffer = Files.newBufferedReader(file)) {
			boolean concidence = buffer.lines().anyMatch(l -> l.contains(strToFind));
			if (concidence) {
				System.out.printf("Valor \"%s\" encontrado en el archivo %s\n", strToFind, file.getFileName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
