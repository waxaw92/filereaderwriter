package com.waxaw.fileprocess.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Clase que permite procesar un archivo y dividirlo en diferentes archivos
 * <i><b>txt</b></i> con contenido de 1000 l&iacute;neas.<br/> <br/>
 * 
 * @author wgavidia
 * @version 2.0 {@code (2019-01-02)}
 */
public class FilePartition implements FileProcessor {

	private static final long serialVersionUID = 1L;
	private static StringBuilder content = new StringBuilder();
	private static int lines = 0;
	private static int idFile = 0;

	/**
	 * Constructor por defecto de la clase.
	 */
	protected FilePartition() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processFile(Path file, Object... args) {
		try (BufferedReader buffer = Files.newBufferedReader(file)) {
			FileProcessor writer = FileProcessorBuilder.build(FileProcessor.FILE_WRITER);

			buffer.lines().filter(l -> !l.isEmpty()).forEach(l -> {
				content.append(l).append("\n");
				lines++;
				if (lines == 1000) {
					idFile++;
					Path newFile = Paths.get("C:\\Users\\wgavidic\\Documents\\files\\" + idFile + ".txt");
					writer.processFile(newFile, content.toString());
					content = new StringBuilder();
					lines = 1;
				}
			});

			if (lines > 0) {
				idFile++;
				Path newFile = Paths.get("C:\\Users\\wgavidic\\Documents\\files\\" + idFile + ".txt");
				writer.processFile(newFile, content.toString());
			}

			System.out.println("Archivos procesados correctamente. " + idFile + " archivos creados.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
