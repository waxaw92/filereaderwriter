package com.waxaw.fileprocess.process;

import java.io.Serializable;
import java.nio.file.Path;

/**
 * Clase abstracta para implementar los diferentes comportamientos al leer un
 * archivo.<br />
 * La instanciaci&oacute;n de las clases se realizan mediante el m&eacute;todo
 * <b>{@code build()}</b> de la f&aacute;brica {@link FileProcessorBuilder}.
 * <br />
 * <br />
 * Clases implementadas:
 * <ul>
 * <li>{@link FilePartition}</li>
 * <li>{@link FileReaderDistinct}</li>
 * <li>{@link FileValidator}</li>
 * <li>{@link FilesValidator}</li>
 * </ul>
 * 
 * @author wgavidia
 * @version 2.0 {@code (2018-12-26)}
 */
public interface FileProcessor extends Serializable {

	int FILE_PARTITIONER = 1;
	int FILE_READER_DISTINCT = 2;
	int FILES_VALIDATOR = 3;
	int FILE_VALIDATOR = 4;
	int FILE_WRITER = 5;

	/**
	 * M&eacute;todo para leer y procesar archivos seg&uacute;n la
	 * implementaci&oacute;n de la clase instanciada.
	 * 
	 * @param file {@link Path} ruta del archivo o directorio a procesar.
	 * @param args {@link Object[]} lista de propiedades para realizar diferentes
	 *             procesos en el archivo.
	 */
	void processFile(Path file, Object... args);

}
