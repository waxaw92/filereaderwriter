package com.waxaw.fileprocess.process;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * Permite crear archivos mediante el m&eacute;todo <b>{@code processFile()}</b>
 * 
 * @author wgavidia
 * @version 2.0 {@code (2019-01-02)}
 */
public class FileWriter implements FileProcessor {

	private static final long serialVersionUID = 5L;

	/**
	 * Constructor por defecto.
	 */
	protected FileWriter() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processFile(Path file, Object... args) {
		if (args == null || args.length < 1) {
			throw new RuntimeException("args params list incomplete. Required String content file to create.");
		}

		try (BufferedWriter writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8, new OpenOption[] { StandardOpenOption.CREATE })) {
			String content = args[0].toString();
			if (!file.toFile().exists())
				file.toFile().createNewFile();
			writer.write(content);
			System.out.println("Archivo " + file.getFileName().toString() + " creado satisfactoriamente.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
