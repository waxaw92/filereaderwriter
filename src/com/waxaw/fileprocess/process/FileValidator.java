package com.waxaw.fileprocess.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Clase para verificar la cantidad de columnas existentes por cada l&iacute;nea
 * de un archivo. Las l&iacute;neas se dividen para verificar la cantidad y se
 * imprimen las l&iacute;neas que no correspondan a la cantidad estipulada.
 * 
 * @author wgavidia
 * @version 2.0 {@code (2019-01-02)}
 *
 */
public class FileValidator implements FileProcessor {

	private static final long serialVersionUID = 4L;
	static int linea = 1;

	protected FileValidator() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processFile(Path file, Object... args) {
		if (args == null || args.length < 2)
			throw new RuntimeException("args params list incomplete. Required String separator and Integer cols.");

		if (!(args[0] instanceof String) || !(args[1] instanceof Integer))
			throw new RuntimeException(
					"args list required with String separator and Integer cols parameters in this order.");

		try (BufferedReader buffer = Files.newBufferedReader(file)) {
			final String separator = String.valueOf(args[0]);
			final int cols = (Integer) args[1];
			buffer.lines().forEach(l -> {
				int columns = l.split(separator).length;
				if (columns != cols)
					System.out.println("linea: " + linea + " | columnas: " + columns);

				linea++;
			});
			System.out.println("Archivos procesados...");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
