package com.waxaw.fileprocess.process;

/**
 * F&aacute;brica para instanciaci&oacute;n de objetos que extienden de la clase
 * {@link FileProcessor}.
 * 
 * @author wgavidia
 * @version 2.0 {@code (2019-01-02)}
 */
public class FileProcessorBuilder {

	/**
	 * Constructor privado de la clase
	 */
	private FileProcessorBuilder() {
	}

	/**
	 * M&eacute;todo para la creaci&oacute;n de objetos.
	 * 
	 * @param type {@code int} valor para identificar la clase que se va a
	 *             instanciar. Se pueden emplear las siguientes constantes definidas
	 *             en la clase {@link FileProcessor}: <b>
	 *             <ul>
	 *             <li>FILE_PARTITIONER</li>
	 *             <li>READ_DISTINCT</li>
	 *             <li>VALIDATE_FILE</li>
	 *             <li>VALIDATE_IN_FILE</li>
	 *             </ul>
	 *             </b>
	 * 
	 * @return Instancia que extienden de la clase {@link FileProcessor}
	 */
	public static FileProcessor build(int type) {
		switch (type) {
		case FileProcessor.FILE_PARTITIONER:
			return new FilePartition();
		case FileProcessor.FILE_READER_DISTINCT:
			return new FileReaderDistinct();
		case FileProcessor.FILES_VALIDATOR:
			return new FilesValidator();
		case FileProcessor.FILE_VALIDATOR:
			return new FileValidator();
		case FileProcessor.FILE_WRITER:
			return new FileWriter();
		default:
			return null;
		}
	}

}
